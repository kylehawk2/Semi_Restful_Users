# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

class UserManager(models.Manager):
    def validation(self, postData):
        errors = {}
        if len(postData['first_name']) < 2:
            errors['first_name'] = "First name must be longer than two characters!"
        if len(postData['last_name']) < 2:
            errors['last_name'] = "Last name must be longer than two characters!"
        if '@' not in postData['email']:
            errors['email'] = "Email must contain a @ symbol!"
        return errors

class User(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = UserManager()
    def __repr__(self):
        return "<User objects: {} {} {}>".format(self.first_name, self.last_name, self.email)

