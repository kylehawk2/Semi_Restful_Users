# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, HttpResponse, redirect

from models import User
from django.contrib import messages

def index(request):
    return render(request, 'users/index.html', { "users" : User.objects.all() })

def new(request):
    return render(request, 'users/new.html')

def create(request):
    errors = User.objects.validation(request.POST)
    if len(errors):
        for tag, error in errors.iteritems():
            messages.error(request, error, extra_tags=tag)
        return redirect('/users/new')
    else:
        new_user = User.objects.create(first_name=request.POST['first_name'], last_name=request.POST['last_name'], email=request.POST['email'])
        return redirect('/users/'+str(new_user.id))

def show(request, id):
    return render(request, 'users/show.html', { "user" : User.objects.get(id=int(id)) })

def edit(request, id):
    return render(request, 'users/edit.html', { "user" : User.objects.get(id=int(id)) })

def update(request, id):
    errors = User.objects.validation(request.POST)
    if len(errors):
        for tag, error in errors.iteritems():
            messages.error(request, error, extra_tags=tag)
        return redirect('/users/'+id+'/edit')
    else:
        User.objects.filter(id=int(id)).update(first_name=request.POST['first_name'], last_name=request.POST['last_name'], email=request.POST['email'])
        return redirect('/users/'+id)

def destroy(request, id):
    User.objects.get(id=int(id)).delete()
    return redirect('/')